package com.huixi.microspur.sysadmin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huixi.microspur.sysadmin.mapper.WjSayingMapper;
import com.huixi.microspur.sysadmin.pojo.entity.saying.WjSaying;
import com.huixi.microspur.sysadmin.service.WjSayingService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 语录表 服务实现类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@Service
public class WjSayingServiceImpl extends ServiceImpl<WjSayingMapper, WjSaying> implements WjSayingService {

}
