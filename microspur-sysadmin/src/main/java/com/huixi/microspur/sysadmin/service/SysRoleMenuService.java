package com.huixi.microspur.sysadmin.service;

import com.huixi.microspur.sysadmin.pojo.entity.sys.SysRoleMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 叶秋
 * @since 2020-07-24
 */
public interface SysRoleMenuService extends IService<SysRoleMenu> {

}
