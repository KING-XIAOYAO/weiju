package com.huixi.microspur.sysadmin.controller.appeal;

import com.huixi.microspur.commons.page.PageData;
import com.huixi.microspur.commons.util.wrapper.Wrapper;
import com.huixi.microspur.sysadmin.pojo.dto.appeal.WjAppealPageDTO;
import com.huixi.microspur.sysadmin.pojo.vo.appeal.QueryAppealVO;
import com.huixi.microspur.sysadmin.service.WjAppealService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(tags = "诉求管理")
@RequestMapping("/wjAppeal")
@RestController
public class WjAppealController {

    @Autowired
    private WjAppealService wjAppealService;

    @ApiOperation(value = "诉求分页查询")
    @GetMapping("/appealPage")
    public Wrapper list(WjAppealPageDTO appealPageDTO) {
        PageData<QueryAppealVO> page = wjAppealService.listPageAppeal(appealPageDTO);
        return Wrapper.ok(page);
    }

    @ApiOperation(value = "单个诉求详情")
    @GetMapping("/{id}")
    public Wrapper get(@PathVariable("id") String appealId) {
        return Wrapper.ok(wjAppealService.appealById(appealId));
    }

    @GetMapping("/list")
    public Wrapper appealPage() {
        System.out.println(" list ");
        return Wrapper.ok();
    }

}
