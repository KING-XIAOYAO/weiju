package com.huixi.microspur.sysadmin.pojo.dto.user;

import lombok.Data;

import java.io.Serializable;

/**
 *  解密加密过后的 用户信息
 * @Author 李辉
 * @Date 2019/11/23 4:49
 * @param
 * @return
 **/
@Data
public class WxDecodeUserInfoDTO implements Serializable {


    /**
     * openId : OPENID
     * nickName : NICKNAME
     * gender : GENDER
     * city : CITY
     * province : PROVINCE
     * country : COUNTRY
     * avatarUrl : AVATARURL
     * unionId : UNIONID
     * watermark : {"appid":"APPID","timestamp":"TIMESTAMP"}
     */

    private String openId;
    private String nickName;
    private Integer gender;
    private String city;
    private String province;
    private String country;
    private String avatarUrl;
    private String unionId;
    private WatermarkBean watermark;

    @Data
    public static class WatermarkBean {
        /**
         * appid : APPID
         * timestamp : TIMESTAMP
         */

        private String appid;
        private String timestamp;

    }
}
