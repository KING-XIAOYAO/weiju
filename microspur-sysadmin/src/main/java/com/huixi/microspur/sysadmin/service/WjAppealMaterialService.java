package com.huixi.microspur.sysadmin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huixi.microspur.sysadmin.pojo.entity.appeal.WjAppealMaterial;

import java.util.List;

/**
 * <p>
 * 诉求素材表-存储素材涉及的图片，或者大文件 服务类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
public interface WjAppealMaterialService extends IService<WjAppealMaterial> {
    
    /**
     *  根据诉求id，查询诉求所有有关的素材（图片，音频，视频）
     * @Author 叶秋 
     * @Date 2020/3/5 0:59
     * @param appealId 诉求id
     * @return java.util.List<com.huixi.microspur.web.pojo.entity.appeal.WjAppealMaterial>
     **/
    List<WjAppealMaterial> listByAppealId(String appealId);


    /**
     *  替换改诉求的素材
     * @Author 叶秋
     * @Date 2020/7/1 21:25
     * @param appealId 诉求id
     * @param materialList 素材列表
     * @return java.lang.Boolean
     **/
    Boolean replaceBeforeMaterial(String appealId, String[] materialList);

}
