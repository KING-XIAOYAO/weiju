package com.huixi.microspur.sysadmin.controller.user;

import com.huixi.microspur.commons.page.PageData;
import com.huixi.microspur.commons.page.PageQuery;
import com.huixi.microspur.commons.util.wrapper.Wrapper;
import com.huixi.microspur.sysadmin.pojo.vo.user.UserDataVO;
import com.huixi.microspur.sysadmin.pojo.vo.user.UserPageVo;
import com.huixi.microspur.sysadmin.service.WjUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 *  小程序用户管理
 * @author wuyiliang
 * @date 2020/8/16
 * @version v1.0
 */
@Api(tags = "小程序用户管理")
@RequestMapping("/wjUser")
@RestController
public class WjUserController {

    @Autowired
    private WjUserService wjUserService;

    /**
     *  根据id查询用户详情
     *
     * @param id userId
     */
    @ApiOperation(value = "小程序用户详情")
    @GetMapping("/{id}")
    public Wrapper<UserDataVO> get(@PathVariable("id") String id) {
        UserDataVO data = wjUserService.getByIdUserData(id);
        return Wrapper.ok(data);
    }

    /**
     *  封禁小程序用户
     *
     * @param id userId
     */
    @ApiOperation(value = "封禁小程序用户")
    @GetMapping("/banUser/{id}")
    public Wrapper banUser(@PathVariable("id") String id) {
        wjUserService.banUser(id);
        return Wrapper.ok();
    }

    /**
     *  小程序用户分页
     *
     * @param pageQuery 分页参数
     */
    @ApiOperation(value = "小程序用户分页")
    @PostMapping("/page")
    public Wrapper<PageData<UserPageVo>> page(@RequestBody PageQuery pageQuery) {
        PageData<UserPageVo> page = wjUserService.page(pageQuery);
        return Wrapper.ok(page);
    }
}
