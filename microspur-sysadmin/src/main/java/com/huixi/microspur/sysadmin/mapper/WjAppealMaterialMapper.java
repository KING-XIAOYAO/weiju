package com.huixi.microspur.sysadmin.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huixi.microspur.sysadmin.pojo.entity.appeal.WjAppealMaterial;

/**
 * <p>
 * 诉求素材表-存储素材涉及的图片，或者大文件 Mapper 接口
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
public interface WjAppealMaterialMapper extends BaseMapper<WjAppealMaterial> {

}
