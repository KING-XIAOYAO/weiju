package com.huixi.microspur.web.pojo.entity.appeal;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.huixi.microspur.commons.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 诉求-对应标签
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("wj_appeal_tag")
@ApiModel(value="WjAppealTag对象", description="诉求-对应标签")
public class WjAppealTag extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "tag_id", type = IdType.ASSIGN_UUID)
    private String tagId;

    @ApiModelProperty(value = "对应的诉求id")
    @TableField("appeal_id")
    private String appealId;

    @ApiModelProperty(value = "标签的名字（冗余）")
    @TableField("tag_name")
    private String tagName;

    @ApiModelProperty(value = "标签的解释（冗余）")
    @TableField("explains")
    private String explains;

    @ApiModelProperty(value = "标签可能需要的地址值(冗余)")
    @TableField("url")
    private String url;

    @ApiModelProperty(value = "创建人")
    @TableField("create_by")
    private String createBy;

    @ApiModelProperty(value = "修改人")
    @TableField("update_by")
    private String updateBy;



}
