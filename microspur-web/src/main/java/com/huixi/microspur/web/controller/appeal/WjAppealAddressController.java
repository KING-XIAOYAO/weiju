package com.huixi.microspur.web.controller.appeal;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.huixi.microspur.commons.base.BaseController;

/**
 * <p>
 * 存储诉求的地址信息 前端控制器
 * </p>
 *
 * @author 叶秋
 * @since 2020-06-30
 */
@RestController
@RequestMapping("/wjAppealAddress")
public class WjAppealAddressController extends BaseController {

}

