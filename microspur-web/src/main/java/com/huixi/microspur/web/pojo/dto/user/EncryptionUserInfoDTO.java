package com.huixi.microspur.web.pojo.dto.user;

import lombok.Data;

import java.io.Serializable;

/**
 * 小程序获取用户信息，加密过后的值。 后端准备接受，准备解密以及存储
 * @Author 李辉
 * @Date 2019/11/22 0:54
 * @param
 * @return
 **/
@Data
public class EncryptionUserInfoDTO implements Serializable {

    /**
     * 不包括敏感信息的原始数据字符串，用于计算签名
     **/
    private String rawData;

    /**
     *  使用 sha1( rawData + sessionkey ) 得到字符串，用于校验用户信息
     **/
    private String signature;

    /**
     *  包括敏感数据在内的完整用户信息的加密数据
     **/
    private String encryptedData;

    /**
     *  加密算法的初始向量
     **/
    private String iv;


    /**
     *  36 位uuid, 对应后端的session_key (用于解密)
     **/
    private String sessionKeyToUuid;


}
