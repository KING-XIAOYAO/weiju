package com.huixi.microspur.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huixi.microspur.commons.page.PageData;
import com.huixi.microspur.web.pojo.vo.chat.WjChatRecordPageVO;
import com.huixi.microspur.web.pojo.entity.chat.WjChatRecord;

/**
 * <p>
 * 聊天室-聊天记录 服务类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
public interface WjChatRecordService extends IService<WjChatRecord> {

    /**
     *  分页查询 聊天记录
     * @Author 叶秋
     * @Date 2020/5/3 12:48
     * @param wjChatRecordPageVO
     * @return java.util.List<com.huixi.microspur.web.pojo.entity.chat.WjChatRecord>
     **/
    PageData queryPageWjChatRecord(WjChatRecordPageVO wjChatRecordPageVO);


    /**
     * 异步保存 聊天记录
     * @Author 叶秋
     * @Date 2020/5/23 21:46
     * @param wjChatRecord
     * @return boolean
     **/
    void asynSaveChatRecord(WjChatRecord wjChatRecord);

}
