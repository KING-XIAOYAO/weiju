package com.huixi.microspur.commons.page;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 分页查询最终传过去的值
 * @Author 叶秋
 * @Date 2020/6/2 21:56
 * @param
 * @return
 **/
@Data
@ApiModel(value = "分页数据")
public class PageData<T> implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "具体的数据")
    private List<T> records;

    @ApiModelProperty(value = "总记录数")
    private Integer total;

    @ApiModelProperty(value = "每页大小")
    private Integer size;

    @ApiModelProperty(value = "当前页")
    private Integer current;


}